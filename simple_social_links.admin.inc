<?php

/**
 * @file
 * Admin page callbacks for the Path Alert module.
 */

/**
 * Menu callback for the Path Alert module to display its administration
 */
function simple_social_links_admin($form, $form_state, $lang = NULL) {

  // Setup the form
  $form['#cache'] = TRUE;

  $simple_social_links_sites = array(
    'twitter' => 'Twitter',
    'facebook' => 'Facebook',
    'google_plus' => 'Google+',
    'linkedin' => 'LinkedIn',
    'pinterest' => 'Pinterest',
  );

  variable_set('simple_social_links_sites', $simple_social_links_sites);

  foreach ($simple_social_links_sites as $key => $val) {
    $form[$key] = array(
      '#type' => 'textfield',
      '#title' => t($val),
      '#default_value' => variable_get($key, ''),
    );
  }

  $form['actions']['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
    '#weight' => 3,
  );

  return $form;
}

/**
 * Triggered when the user submits the administration page.
 */
function simple_social_links_admin_submit($form, &$form_state) {
  foreach (variable_get('simple_social_links_sites', array()) as $key => $val) {
    variable_set($key, $form_state['input'][$key]);
  }
  drupal_set_message(t('Your changes have been saved.'));
}

